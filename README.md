# OD-Suite

> A collection of agnostic webcomponents

`OD-Suite` is a set of agnostic components that will help you build faster web apps and organize your data more efficiently. It currently contains 10 major components ranging from simple buttons to take you to the top of the page to complex forms frameworks designed to quickly and acurately handle user input data while taking semantic html forms leg-work off of the developer.

## Installation
- Install with [npm](https://www.npmjs.com/)

```
npm i @orcden/od-suite
```
## Usage
```
import '@orcden/od-suite';
```
```
<od-cabinet>
    <od-cabinet-item>
        <span slot='title'>Spinner</span>
        <div style='position: relative;height: 200px;width: 100%;'>
            <od-spinner scoped='true' active='true' ></od-spinner>
        </div>
    </od-cabinet-item>
</od-cabinet>
<od-top-button offset=5></od-top-button>
```
    
## Components
 - [<od-cabinet>](https://bitbucket.org/OrcDen/od-cabinet/src/master/)
 - [<od-carousel>](https://bitbucket.org/OrcDen/od-carousel/src/master/)
 - [<od-forms>](https://bitbucket.org/OrcDen/od-forms/src/master/)
 - [<od-gallery>](https://bitbucket.org/OrcDen/od-gallery/src/master/)
 - [<od-modal>](https://bitbucket.org/OrcDen/od-modal/src/master/)
 - [<od-richtext>](https://bitbucket.org/OrcDen/od-richtext/src/master/)
 - [<od-spinner>](https://bitbucket.org/OrcDen/od-spinner/src/master/)
 - [<od-top-button>](https://bitbucket.org/OrcDen/od-top-button/src/master/)
 - [<od-smart-list>](https://bitbucket.org/OrcDen/od-smart-list/src/master/)
 - [<od-wizard>](https://bitbucket.org/OrcDen/od-wizard/src/master/)

## Development
### Run development server and show demo

```
npm run demo
```

### Run linter

```
npm run lint
```

### Fix linter errors

```
npm run fix
```

### Build for production

```
npm run build
```